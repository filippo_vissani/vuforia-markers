# Vuforia Markers Tests
Progetto Unity con Vuforia e componenti MRTK utilizzato per effettuare test su:

*	Sistemi di coordinate con marker.
*	Stabilità degli ologrammi.
*	Connessione con server e trasferimento dati attraverso la rete.

## Versioni utilizzate
*	Unity 2020.3.4f1
*	Plugin Windows XR 4.4.2
*	Vuforia 9.8.5
*	Visual Studio 2019

## Descrizione
All'interno della Scene di Unity sono presenti i seguenti GameObject:

*	Un marker Vuforia.
*	Una sfera azzurra posizionata all'origine del mondo.
*	Cubi arancioni inseriti come figli del marker, e quindi la loro posizione viene espressa in coordinate rispetto al marker.
*	Cubi rosso, verde, blu e azzurro inseriti non come figli del marker, quindi hanno coordinate espresse rispetto all'origine del mondo. Quando il marker viene rilevato la posizione di questi cubi viene modificata, mantenendo le coordinate rispetto all'origine. Il cubo azzurro può essere spostato dall'utente (Object Manipulator).
*	Label (TextMeshPro) utilizzate per visualizzare la posizione degli ologrammi rispetto all'origine.
*	ConnectionManager utilizzato per gestire la connessione con il [Mixed Reality Server](https://bitbucket.org/filippo_vissani/mixed-reality-server/src/master/).

## Funzionamento
Una volta avviata l'applicazione, all'interno dell'ambiente saranno presenti solo la sfera e i cubi rosso, verde, blu e azzurro (con il quale è possibile interagire), con posizione stabilita da Unity.
Quando il marker viene rilevato vengono generati i cubi arancioni e i cubi già presenti vengono riposizionati.
La posizione del cubo azzurro viene inviata al server per ogni spostamento superiore a 30cm su ogni asse.
Prima di inviare la posizione viene fatta una sottrazione fra la posizione effettiva del cubo e la posizione del marker, sfruttando quindi il marker come punto di riferimento.
Nei test effettuati gli ologrammi con coordinate espresse rispetto all'origine della Scene sono risultati più stabili rispetto a quelli inseriti come figli del marker.
Muovendosi all'interno dell'ambiente la posizione dei cubi arancioni viene modificata, commettendo un errore che può arrivare fino a 20cm.
