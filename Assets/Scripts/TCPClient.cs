using System;
using System.Collections;
using System.Net.Sockets;
using TMPro;
using UnityEngine;

public class TCPClient : MonoBehaviour
{
    public TextMeshPro debug;
    public GameObject childGameObject;
    public GameObject parentGameObject;
    private Vector3 lastPosition;
    private NetworkStream stream;
    private TcpClient client;
    // Start is called before the first frame update
    void Start()
    {
        lastPosition = childGameObject.transform.position;
        IEnumerator coroutine = StartConnection();
        StartCoroutine(coroutine);
    }

    // Update is called once per frame
    void Update()
    {
        if (Math.Abs(lastPosition.x - childGameObject.transform.position.x) > 0.3
            || Math.Abs(lastPosition.y - childGameObject.transform.position.y) > 0.3
            || Math.Abs(lastPosition.z - childGameObject.transform.position.z) > 0.3)
        {
            SendPosition();
            lastPosition = childGameObject.transform.position;
        }
    }

    private IEnumerator StartConnection()
    {
        // Create a TcpClient.
        // Note, for this client to work you need to have a TcpServer
        // connected to the same address as specified by the server, port
        // combination.
        Int32 port = 13000;
        client = new TcpClient("192.168.40.100", port);

        // Get a client stream for reading and writing.
        // Stream stream = client.GetStream();
        stream = client.GetStream();

        yield return null;
    }

    private IEnumerator CloseConnection()
    {
        // Close everything.
        stream.Close();
        client.Close();

        yield return null;
    }

    private IEnumerator SendTCPMessage(String message)
    {
        // Translate the passed message into ASCII and store it as a Byte array.
        Byte[] data = System.Text.Encoding.ASCII.GetBytes(message);

        // Send the message to the connected TcpServer.
        stream.Write(data, 0, data.Length);

        //debug.SetText("Sent: {0}" + message);

        // Receive the TcpServer.response.

        // Buffer to store the response bytes.
        data = new Byte[256];

        // String to store the response ASCII representation.
        String responseData = String.Empty;

        // Read the first batch of the TcpServer response bytes.
        Int32 bytes = stream.Read(data, 0, data.Length);
        responseData = System.Text.Encoding.ASCII.GetString(data, 0, bytes);
        //debug.SetText("Received: {0}" + responseData);

        yield return null;
    }

    public void SendPosition()
    {
        Vector3 position = childGameObject.transform.position - parentGameObject.transform.position;
        IEnumerator coroutine = SendTCPMessage(position.ToString());
        StartCoroutine(coroutine);
    }
}
