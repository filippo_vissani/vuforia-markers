using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PrintPosition : MonoBehaviour
{
    public TextMeshPro text;
    public GameObject root;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        text.SetText(root.transform.position.ToString());
    }
}
