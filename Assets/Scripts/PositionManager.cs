using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class PositionManager : MonoBehaviour
{
    public GameObject root;
    private bool check;

    void Start()
    {
        check = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (check == false)
        {
            // Get the Vuforia StateManager
            StateManager sm = TrackerManager.Instance.GetStateManager();

            // Query the StateManager to retrieve the list of
            // currently 'active' trackables 
            //(i.e. the ones currently being tracked by Vuforia)
            IEnumerable<TrackableBehaviour> activeTrackables = sm.GetActiveTrackableBehaviours();

            // Iterate through the list of active trackables
            foreach (TrackableBehaviour tb in activeTrackables)
            {
                root.transform.position = tb.transform.position;
                root.transform.rotation = tb.transform.rotation;
                check = true;
            }
        }
    }
}
