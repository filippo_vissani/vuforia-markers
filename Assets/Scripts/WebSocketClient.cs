using UnityEngine;

using NativeWebSocket;
using TMPro;
using System;
using Newtonsoft.Json.Linq;

public class WebSocketClient : MonoBehaviour
{
    public TextMeshPro debug;
    public GameObject childGameObject;
    public GameObject parentGameObject;
    private Vector3 lastPosition;
    private WebSocket ws;
    async void Start()
    {
        ws = new WebSocket("ws://192.168.40.100:8080");

        ws.OnMessage += (bytes) =>
        {
            debug.SetText("Message received!");
            //JObject data = JObject.Parse(System.Text.Encoding.UTF8.GetString(bytes));
            //Vector3 newPosition = new Vector3(data + parentGameObject.transform.position.x,
            //    data.Y + parentGameObject.transform.position.y,
            //    data.Z + parentGameObject.transform.position.z);
            //lastPosition = newPosition;
            //childGameObject.transform.position = newPosition;
        };

        ws.OnOpen += () =>
        {
            debug.SetText("Connected!\n" + ws.State.ToString());
        };

        ws.OnError += (e) =>
        {
            debug.SetText(e);
        };

        await ws.Connect();
    }
    void Update()
    {
        if (ws == null)
        {
            return;
        }
        if (Math.Abs(lastPosition.x - childGameObject.transform.position.x) > 0.3
            || Math.Abs(lastPosition.y - childGameObject.transform.position.y) > 0.3
            || Math.Abs(lastPosition.z - childGameObject.transform.position.z) > 0.3)
        {
            SendPosition();
        }
    }

    private async void SendPosition()
    {
        Vector3 position = childGameObject.transform.position - parentGameObject.transform.position;
        string message = "{\"X\":" + position.x
                        + ", \"Y\":" + position.y
                        + ", \"Z\":" + position.z + "}";
        await ws.SendText(message);
        lastPosition = childGameObject.transform.position;
    }

    private async void OnApplicationQuit()
    {
        await ws.Close();
    }
}
